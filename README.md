
    Please note: 
    This repository was moved to github: https://github.com/tomsrocket/1d-bobble



1D-Bobble
=========

This small game was written during a Global Game Jam in 2012.
Audio output is very buggy and makes the game crash. Don't enable it :-p

Demo : http://bobble.input23.de/

Features
--------

- Shoot monsters with your green bubbles, and then pop them!
- More than 10 stunning levels
- Only 1 Dimension!

ToDo
----

See "Issues"

Issues
------

See "ToDo"

